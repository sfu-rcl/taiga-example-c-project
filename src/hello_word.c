#include "board_support.h"
#include <stdio.h>

int main(void) {
    //Platform Initialization
    platform_init ();

    //Records cycle and instruction counts
    start_profiling ();

    printf("Hello world!\n");

    //Records cycle and instruction counts
    //Prints summary stats for the application
    end_profiling ();

    return 0;
}